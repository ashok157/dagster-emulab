from __future__ import print_function

import os
import sys
import tempfile
import time
import json
import lxml.etree
import itertools
import copy

import emulab_sslxmlrpc.client.api as emulab_api
import emulab_sslxmlrpc.xmlrpc as emulab_xmlrpc

def convert(p,v):
    if v in [ "True","true",True ]:
        return True
    elif v in  [ "False","false",False ]:
        return False
    #elif v == None:
    #    return ""
    return v

def parse_manifest(manifest):
    root = lxml.etree.fromstring(manifest)

    facts_dict = dict()

    # Find nodes
    nodes = dict()
    for elm in root.getchildren():
        if not elm.tag.endswith("}node"):
            continue
        (host,ip) = (None,None)
        for elm2 in elm.getchildren():
            if elm2.tag.endswith("}host"):
                (host,ip) = (elm2.get("name",None),elm2.get("ipv4",None))
                break
        nodes[elm.get("client_id")] = dict(hostname=host,ip=ip)
    facts_dict["nodes"] = nodes
    facts_dict["nodenames"] = list(nodes)

    # Find parameters
    parameters = dict()
    for elm in root.getchildren():
        if elm.tag.endswith("}data_set"):
            for elm2 in elm.getchildren():
                if elm2.tag.endswith("}data_item"):
                    p = elm2.get("name")
                    v = convert(p,elm2.text)
                    parameters[p] = v
        if elm.tag.endswith("}data_item"):
            p = elm.get("name")
            parameters[p] = convert(p,elm.text)
    facts_dict["parameters"] = parameters

    # Find all the public IP addresses
    routable_pools = dict()
    for elm in root.getchildren():
        if not elm.tag.endswith("}routable_pool"):
            continue
        pool_name = elm.get("client_id")
        if not pool_name in routable_pools:
            routable_pools[pool_name] = []
        for elm2 in elm.getchildren():
            if elm2.tag.endswith("}ipv4"):
                routable_pools[pool_name].append(
                    [elm2.get("address"),elm2.get("netmask")])
    facts_dict["routable_pools"] = routable_pools

    # Dig through ansible extensions and maybe generate some inventories,
    # overrides, and playbook wrappers.  First we grab the roles/collections/playbooks,
    # then the bindings, then the overrides, then the script.
    roles = dict()
    for elm in root.getchildren():
        if not elm.tag.endswith("}role"):
            continue
        playbook_order = []
        playbooks = dict()
        roles[elm.get("name")] = dict(
            path=elm.get("path"),source=elm.get("source",""),
            group=elm.get("group",""),playbooks=playbooks,playbook_order=playbook_order,nodes=[])
        for elm2 in elm.getchildren():
            if not elm2.tag.endswith("}playbook"):
                continue
            pname = elm2.get("name")
            playbook_order.append(pname)
            playbooks[pname] = dict(
                path=elm2.get("path"),inventory_name=elm2.get("inventory_name"),
                overrides_name=elm2.get("overrides_name"),
                inventory_generator_path=elm2.get("inventory_generator_path"),
                overrides_generator_path=elm2.get("overrides_generator_path"),
                runner_path=elm2.get("runner_path"),pre_hook=elm2.get("pre_hook"),
                post_hook=elm2.get("post_hook"),become=elm2.get("become"))
    facts_dict["roles"] = roles

    role_bindings = dict()
    for elm in root.getchildren():
        bound_roles = []
        if not elm.tag.endswith("}node"):
            continue
        nodename = elm.get("client_id")
        for elm2 in elm.getchildren():
            if not elm2.tag.endswith("}role_binding"):
                continue
            role = elm2.get("role")
            bound_roles.append(role)
            if role in roles:
                roles[role]["nodes"].append(nodename)
        if bound_roles:
            role_bindings[elm.get("client_id")] = bound_roles
    facts_dict["role_bindings"] = role_bindings

    global_overrides = dict()
    pernode_overrides = dict()
    for elm in root.getchildren():
        if elm.tag.endswith("}override"):
            source = elm.get("source")
            lsource = source + "s"
            source_name = elm.get("source_name")
            value = None
            if lsource in facts_dict:
                value = facts_dict[lsource].get(source_name,None)
            elif not source:
                value = elm.text
            global_overrides[elm.get("name")] = dict(
                source=lsource,source_name=source_name,value=value)
        if elm.tag.endswith("}node"):
            nodename = elm.get("client_id")
            for elm2 in root.getchildren():
                if elm2.tag.endswith("}override"):
                    name2 = elm2.get("name")
                    source2 = elm2.get("source")
                    lsource2 = source2 + "s"
                    source_name2 = elm2.get("source_name")
                    value2 = None
                    if lsource2 in facts_dict:
                        value2 = facts_dict[lsource2].get(source_name2,None)
                    elif not source2:
                        value2 = elm2.text
                    if not nodename in pernode_overrides:
                        pernode_overrides[nodename] = dict()
                    pernode_overrides[nodename][name2] = dict(
                        source=lsource2,source_name=source_name2,value=value2)
    facts_dict["global_overrides"] = global_overrides
    facts_dict["pernode_overrides"] = pernode_overrides

    return facts_dict

class EmulabBaseAction():

    APIMAP = {
        "experiment.create": "startExperiment",
        "experiment.status": "experimentStatus",
        "experiment.terminate": "terminateExperiment",
        "experiment.connect": "connectExperiment",
        "experiment.disconnect": "disconnectExperiment",
        "util.itertools.product": "product",
        "util.dict.merge": "merge",
    }

    def __init__(self, config):

        # Python ssl lib cannot be given cert/key info via string, only file,
        # because of how it wraps the underlying thing (e.g. openssl).
        certificate_content = config.get("certificate")
        (fd,certificate_file) = tempfile.mkstemp(text=True)
        f = os.fdopen(fd,"w")
        f.write(certificate_content)
        f.close()

        self.emulab_config = dict(
            debug=config.get("debug", False),
            certificate=certificate_file
        )
        server = config.get("server",None)
        port = config.get("port",None)
        path = config.get("path",None)
        if server:
            self.emulab_config["server"] = server
        if port:
            self.emulab_config["port"] = port
        if path:
            self.emulab_config["path"] = path
        self.emulab_rpc = emulab_xmlrpc.EmulabXMLRPC(self.emulab_config)
        self.sshpubkey = config.get("sshpubkey",None)

        os.unlink(certificate_file)

    def run(self, **kwargs):
        action_name = kwargs.pop("action")

        if not action_name in self.APIMAP:
            print("Unknown action %r",action_name,file=sys.stderr)
            exit(1)

        return getattr(self,self.APIMAP[action_name])(kwargs)

    def _handle_result_code(self, result_code, response, errors_are_fatal=True):
        pass

    def startExperiment(self, params):
        real_params = dict()
        for (k,v) in params.items():
            if k in [ "profile", "proj", "name", "aggregate", "duration",
                      "start", "stop", "refspec", "paramset" ]:
                if k not in params or params[k] is None:
                    continue
                real_params[k] = v
        #
        # We want to use a default sshpubkey supplied in the pack config unless
        # overridden by the user.  If the user overrides with the empty string
        # we send nothing.  This default would have to change on a multiuser
        # install of this pack; would not want a default pubkey there.  But that
        # is up to the pack user to do the right thing; this gives them the choice.
        #
        if "sshpubkey" in params and params["sshpubkey"] is not None:
            if params["sshpubkey"]:
                real_params["sshpubkey"] = params["sshpubkey"]
        elif self.sshpubkey:
            real_params["sshpubkey"] = self.sshpubkey

        if "bindings" in params:
            real_params["bindings"] = json.dumps(params["bindings"])

        (result_code,response) = emulab_api.startExperiment(self.emulab_rpc, real_params).apply()
        output = ""
        if response is not None:
            output = response.output
        if result_code != emulab_xmlrpc.RESPONSE_SUCCESS:
            if params["errors_are_fatal"]:
                print("startExperiment error (result_code=%r): %r" % (
                    result_code,output),
                    file=sys.stderr)
                exit(result_code)
            else:
                return dict(params=real_params,result_code=result_code,error=output)
        if params["wait_for_status"]:
            status_params = dict(**params,asjson=True)
            ret = self.experimentStatus(status_params)
            if params["errors_are_fatal"] and ret["status"]["status"] == "failed":
                print("startExperiment error (failure_code=%r): %r" % (
                    ret["status"].get("failure_code","unknown"),
                    ret["status"].get("failure_message","unknown")), file=sys.stderr)
                exit(1)
            ret["params"] = real_params
            return ret
        return dict(params=real_params,result_code=result_code,error=output)

    def experimentStatus(self, params):
        real_params = dict(
            experiment="%s,%s" % (params["proj"],params["name"]),withcert=1,
            asjson=True
        )
        url_printed = False
        wait_start = time.time()
        (result_code,response,status) = (None,None,None)
        while (time.time() - wait_start) < params["max_wait_time"]:
            (result_code,response) = emulab_api.experimentStatus(self.emulab_rpc, real_params).apply()
            if response:
                output = response.output
            else:
                output = ""
            if result_code != emulab_xmlrpc.RESPONSE_SUCCESS:
                if params["errors_are_fatal"]:
                    print("experimentStatus error (result_code=%r): %r",
                          result_code,output,file=sys.stderr)
                    exit(result_code)
                else:
                    return dict(result_code=result_code,error=output)
            status = json.loads(response.value)
            if "context" in params:
                if not url_printed:
                    params["context"].log.info(status["url"])
                    url_printed = True
                    pass
                pass
            
            if not params["wait_for_status"]:
                break
            if status["status"] in [ "ready", "failed" ]:
                if status["status"] == "failed":
                    return dict(result_code=1,error="",status=status)
                elif not "execute_status" in status:
                    # No execute services, so no point to further waits
                    break
                elif not params["wait_for_execute_status"]:
                    # Not told to wait for execute_status, so no further waits
                    break
                elif status["execute_status"]["running"] == 0:
                    # Told to wait for execute_status and services are no longer running
                    break
                else:
                    # Still waiting for execute_status to have no running services
                    continue
            else:
                (result_code,response,status) = (None,None,None)
                time.sleep(params["interval"])
        if result_code == None:
            print("experimentStatus timed out",file=sys.stderr)
            exit(1)

        # Grab manifests quickly
        manifests = dict()
        try:
            (manifests_result_code,manifests_response) = \
              emulab_api.experimentManifests(self.emulab_rpc, real_params).apply()
            manifests = json.loads(manifests_response.value)
        except:
            pass

        manifest_facts = dict()
        try:
            for (cm,manifest) in manifests.items():
                manifest_facts[cm] = parse_manifest(manifest)
        except:
            pass

        return dict(result_code=result_code,error="",status=status,manifests=manifests,
                    manifest_facts=manifest_facts)

    def terminateExperiment(self, params):
        real_params = dict(
            experiment="%s,%s" % (params["proj"],params["name"])
        )
        (result_code,response) = emulab_api.terminateExperiment(self.emulab_rpc, real_params).apply()
        if response:
            output = response.output
        else:
            output = ""
        if result_code != emulab_xmlrpc.RESPONSE_SUCCESS:
            if params["errors_are_fatal"]:
                print("terminateExperiment error (result_code=%r): %r",
                      result_code,output,file=sys.stderr)
                exit(result_code)
            else:
                return dict(result_code=result_code,error=output)
        else:
            return dict(result_code=result_code)

    def connectExperiment(self, params):
        real_params = dict(
            experiment=params["experiment"],
            sourcelan=params["lan"],
            targetexp=params["target_experiment"],
            targetlan=params["target_shared_vlan"]
        )
        (result_code,response) = emulab_api.connectExperiment(self.emulab_rpc, real_params).apply()
        output = ""
        if response is not None:
            output = response.output
        if result_code != emulab_xmlrpc.RESPONSE_SUCCESS:
            if params["errors_are_fatal"]:
                print("connectSharedLan error (result_code=%r): %r" % (
                    result_code,output),
                    file=sys.stderr)
                exit(result_code)
        return dict(params=real_params,result_code=result_code,error=output)

    def disconnectExperiment(self, params):
        real_params = dict(
            experiment=params["experiment"],
            sourcelan=params["lan"]
        )
        (result_code,response) = emulab_api.disconnectExperiment(self.emulab_rpc, real_params).apply()
        output = ""
        if response is not None:
            output = response.output
        if result_code != emulab_xmlrpc.RESPONSE_SUCCESS:
            if params["errors_are_fatal"]:
                print("disconnectSharedLan error (result_code=%r): %r" % (
                    result_code,output),
                    file=sys.stderr)
                exit(result_code)
        return dict(params=real_params,result_code=result_code,error=output)

    def product(self, params):
        return dict(product=list(itertools.product(*params.get("iterables"))))

    def merge(self, params):
        res = dict()
        inputlist = params.get("dicts",[])
        for d in inputlist:
            res.update(copy.deepcopy(d))
        modified = False
        if inputlist:
            modified = inputlist[0] != res
        return dict(result=res,modified=modified)
